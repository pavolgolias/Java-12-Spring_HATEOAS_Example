package hello.model;

import hello.GreetingController;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class Bookmark extends ResourceSupport {
    private String uri;
    private String description;

    Bookmark() {
    }

    public Bookmark(String uri, String description) {
        this.uri = uri;
        this.description = description;

        add(linkTo(methodOn(GreetingController.class).bookmark()).withSelfRel());
        add(linkTo(methodOn(GreetingController.class).greeting(null)).withRel("link-to-greetings"));
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
