package hello.dummy;

import hello.model.Bookmark;

import java.util.ArrayList;
import java.util.List;

public class BookmarkDummyData {
    public static final List<Bookmark> BOOKMARK_LIST = new ArrayList<>();

    static {
        BOOKMARK_LIST.add(new Bookmark("http://bookmark.com/1/", "A description 1"));
        BOOKMARK_LIST.add(new Bookmark("http://bookmark.com/2/", "A description 2"));
        BOOKMARK_LIST.add(new Bookmark("http://bookmark.com/3/", "A description 3"));
    }

}
